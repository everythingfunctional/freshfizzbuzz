module divisible_by_three_test
    implicit none
    private

    public :: test_for_divisible_by_three
contains
    function test_for_divisible_by_three() result(tests)
        use Vegetables_m, only: TestItem_t, describe, it

        type(TestItem_t) :: tests

        tests = describe("Fizzbuzz for numbers divisible by three", &
                [it("returns 'fizz'", checkDivisbleByThreeNumbers)])
    end function test_for_divisible_by_three

    function checkDivisbleByThreeNumbers() result(result_)
        use fizzbuzz_m, only: fizzbuzz
        use Vegetables_m, only: Result_t, assertEquals

        type(Result_t) :: result_

        result_ = &
                assertEquals("fizz", fizzbuzz(3)) &
                .and.assertEquals("fizz", fizzbuzz(6))
    end function checkDivisbleByThreeNumbers
end module divisible_by_three_test
