module fizzbuzz_m
    implicit none
    private

    public :: fizzbuzz
contains
    function fizzbuzz(number) result(string)
        integer, intent(in) :: number
        character(len=:), allocatable :: string

        character(len=32) :: temp

        if (mod(number, 15) == 0) then
            string = "fizzbuzz"
        else if (mod(number, 5) == 0) then
            string = "buzz"
        else if (mod(number, 3) == 0) then
            string = "fizz"
        else
            write(temp, '(I0)') number
            string = trim(temp)
        end if
    end function fizzbuzz
end module fizzbuzz_m
