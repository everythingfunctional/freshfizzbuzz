module divisible_by_five_test
    implicit none
    private

    public :: test_for_divisible_by_five
contains
    function test_for_divisible_by_five() result(tests)
        use Vegetables_m, only: TestItem_t, describe, it

        type(TestItem_t) :: tests

        tests = describe("Fizzbuzz for numbers divisible by five", &
                [it("returns 'buzz'", checkDivisbleByFiveNumbers)])
    end function test_for_divisible_by_five

    function checkDivisbleByFiveNumbers() result(result_)
        use fizzbuzz_m, only: fizzbuzz
        use Vegetables_m, only: Result_t, assertEquals

        type(Result_t) :: result_

        result_ = &
                assertEquals("buzz", fizzbuzz(5)) &
                .and.assertEquals("buzz", fizzbuzz(10))
    end function checkDivisbleByFiveNumbers
end module divisible_by_five_test
