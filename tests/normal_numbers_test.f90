module normal_numbers_test
    implicit none
    private

    public :: test_fizzbuzz_for_normal_numbers
contains
    function test_fizzbuzz_for_normal_numbers() result(tests)
        use Vegetables_m, only: TestItem_t, describe, it

        type(TestItem_t) :: tests

        tests = describe("Fizzbuzz for normal numbers", &
                [it("returns that number as a string", checkNormalNumbers)])
    end function test_fizzbuzz_for_normal_numbers

    function checkNormalNumbers() result(result_)
        use fizzbuzz_m, only: fizzbuzz
        use Vegetables_m, only: Result_t, assertEquals

        type(Result_t) :: result_

        result_ = &
                assertEquals("1", fizzbuzz(1)) &
                .and.assertEquals("2", fizzbuzz(2))
    end function checkNormalNumbers
end module normal_numbers_test
