module divisible_by_fifteen_test
    implicit none
    private

    public :: test_for_divisible_by_fifteen
contains
    function test_for_divisible_by_fifteen() result(tests)
        use Vegetables_m, only: TestItem_t, describe, it

        type(TestItem_t) :: tests

        tests = describe("Fizzbuzz for numbers divisible by fifteen", &
                [it("returns 'fizzbuzz'", checkDivisbleByFifteenNumbers)])
    end function test_for_divisible_by_fifteen

    function checkDivisbleByFifteenNumbers() result(result_)
        use fizzbuzz_m, only: fizzbuzz
        use Vegetables_m, only: Result_t, assertEquals

        type(Result_t) :: result_

        result_ = &
                assertEquals("fizzbuzz", fizzbuzz(15)) &
                .and.assertEquals("fizzbuzz", fizzbuzz(30))
    end function checkDivisbleByFifteenNumbers
end module divisible_by_fifteen_test
